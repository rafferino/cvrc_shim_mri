close all;
clear all;

%%
data = load("./data/3-4_scan/nocurrent_Fieldmap.mat");
% data = load("./data/DataStruct.mat");
[resolution_x, resolution_y, num_slices] = size(data.PhaseMap)
slice_num = 2;%floor(num_slices/2)

%% Load fieldmaps of induced magnetic fields from loops
% [fieldmap, least_sq_b, loop_matrix, loop_matrix_masked, mask] = load_magnetic_loops("./data/11-3_3 scans", size(data.PhaseMap), slice_num, "data\bottle_12loops_slice40_L2_Fieldmap_4", true);
[fieldmap, least_sq_b, loop_matrix, loop_matrix_masked, mask] = load_magnetic_loops("./data/3-4_scan", size(data.PhaseMap), slice_num, "", true);

%% Mask and fit the field map to the loops
% [fieldmap, loop_matrix_masked, least_sq_b, mask] = loop_fieldmap_masking(loop_matrix, data, slice_num);
x = loop_fieldmap_fitting(loop_matrix_masked, least_sq_b, 2);

%%
% Shim before-after plotting
% calculate loaded loop induced magnetic fields
loop_fieldmap = reshape(loop_matrix*(x), [resolution_x, resolution_y]).*mask;
original_fieldmap = reshape(fieldmap, [resolution_x, resolution_y]).*mask;
shimmed = -loop_fieldmap + fieldmap; %the full difference
% 
% % calculate min and max for plotting bounds
low = min(min(fieldmap, [], 'all'), min(-loop_fieldmap+fieldmap, [], 'all'))
high = max(max(max(fieldmap)), max(max(-loop_fieldmap+fieldmap)))
% 
% % plotting
value = 1000;
figure('Name', 'Shimming');
subplot(1, 3, 1);
imshow(loop_fieldmap.*mask, [-value value]);
title("Loop Generated Fieldmap");
colormap hot;
colorbar;
subplot(1, 3, 2);
imshow(original_fieldmap.*mask, [-value value]);
title("Original Fieldmap");
colormap hot;
colorbar;
subplot(1, 3, 3);
imshow(shimmed.*mask, [-value value]);
title("Sum");
colormap hot;
colorbar;

%% Histogram plotting

% hist([norm_loops*x-least_sq_b, least_sq_b], 100)
%the masked difference between the fit values and the scan
figure('Name', 'Histogram');
subplot(1,3,1);
h1 = histogram(loop_matrix_masked*x-least_sq_b); %100 bins
h1.BinWidth
h1.BinLimits = [-10e-6, 10e-6];
h1.NumBins = 100;
h1.FaceColor = 'r';
h1.EdgeColor = 'none';
title("Fitted");

subplot(1,3,2);
h2 = histogram(least_sq_b);
h2.BinLimits = [-10e-6, 10e-6];
h2.NumBins = 100;
h2.FaceColor = 'b';
h2.EdgeColor = 'none';
title("Unfitted");

subplot(1,3,3);
h3 = histogram(loop_matrix_masked*x-least_sq_b);
h3.BinLimits = [-10e-6, 10e-6];
h3.NumBins = 100;
h3.FaceColor = 'r';
h3.EdgeColor = 'none';
hold on;
h4 = histogram(least_sq_b);
h4.BinLimits = [-10e-6, 10e-6];
h4.NumBins = 100;
h4.FaceColor = 'b';
h4.EdgeColor = 'none';
title("Both");

%% Figure making


