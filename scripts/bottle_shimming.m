%---------------------------------------------------
%  NAME:      example_3D_solenoid_filament.m
%  WHAT:      Calculation of the magnetic field of a solenoid
%             on a volume (+ 3D plot).
%  REQUIRED:  BSmag Toolbox 20150407
%  AUTHOR:    20150407, L. Queval (loic.queval@gmail.com)
%  COPYRIGHT: 2015, Loic Qu�val, BSD License (http://opensource.org/licenses/BSD-3-Clause).
%----------------------------------------------------
%load('~/Desktop/feb19_scan/DataStruct.mat');
% Initialize
clear all; 
% close all;
clc;
% BSmag = BSmag_init(); % Initialize BSmag analysis

% Source points (where there is a current source)
%r = 1/5 + 0.03 * sin(4 * theta') + 0.05 * sin(2 * theta' - 0.5);
%Gamma = [r .* cos(theta'), r .* sin(theta'), theta'/10000]; % x,y,z [m,m,m]
num_loops = 6;
theta_resolution = 200;
theta = linspace(-pi,pi,theta_resolution);
Gamma = zeros(theta_resolution,3,num_loops);
size_divider = 45;
y_sep = 55;
near_offset = 30;
far_offset = 15;
Gamma(:,:,1) = [sin(theta')/size_divider + 1/(-near_offset), cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
Gamma(:,:,2) = [sin(theta')/size_divider, cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
Gamma(:,:,3) = [sin(theta')/size_divider + 1/(near_offset), cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
Gamma(:,:,4) = [sin(theta')/size_divider + 1/(-near_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
Gamma(:,:,5) = [sin(theta')/size_divider, cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
Gamma(:,:,6) = [sin(theta')/size_divider + 1/(near_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
% Gamma(:,:,7) = [sin(theta')/size_divider + 1/(far_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
% Gamma(:,:,8) = [sin(theta')/size_divider + 1/(near_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];

% size_divider = 45;
% y_sep = 55;
% near_offset = 30;
% far_offset = 15;
% Gamma(:,:,1) = [zeros(size(theta')), zeros(size(theta')), zeros(size(theta'))];
% Gamma(:,:,2) = [zeros(size(theta')), zeros(size(theta')), zeros(size(theta'))];
% Gamma(:,:,3) = [sin(theta')/size_divider + 1/(near_offset), cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
% Gamma(:,:,4) = [zeros(size(theta')), zeros(size(theta')), zeros(size(theta'))];
% Gamma(:,:,5) = [sin(theta')/size_divider, cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
% Gamma(:,:,6) = [zeros(size(theta')), zeros(size(theta')), zeros(size(theta'))];


I = 1; % filament current [A]
dGamma = 1e9; % filament max discretization step [m]
x_dim = .1;
y_dim = .1;
z_dim = .05;
resolution_z = 5;
resolution_xy = 256;

loop_matrix = zeros(resolution_xy*resolution_xy, num_loops);
BZ_slice=cell(1,num_loops);
norms = cell(1, num_loops);
fields = cell(1, num_loops);
for loop_num = 1:num_loops
    BSmag.Nfilament = 0;
    [BSmag] = BSmag_add_filament(BSmag,Gamma(:,:,loop_num),I,dGamma);


    % Field points (where we want to calculate the field)
    x_M = linspace(-x_dim/2,x_dim/2,resolution_xy); % x [m]
    y_M = linspace(-y_dim/2,y_dim/2,resolution_xy); % y [m]
    z_M = linspace(-z_dim/2,z_dim/2,resolution_z); % z [m]
    [X_M,Y_M,Z_M]=meshgrid(x_M,y_M,z_M);
%     BSmag_plot_field_points(BSmag,X_M,Y_M,Z_M); % shows the field points volume

    % Biot-Savart Integration
    [BSmag,X,Y,Z,BX,BY,BZ] = BSmag_get_B(BSmag,X_M,Y_M,Z_M);
    field.BSmag = BSmag;
    field.X = X;
    field.Y = Y;
    field.Z = Z;
    field.BX = BX;
    field.BY = BY; 
    field.BZ = BZ;
    fields{loop_num} = field;
    
    BZ_slice{loop_num} = BZ(:,1:resolution_xy,4);
    
    
    loop_matrix(:,loop_num) = reshape(BZ_slice{loop_num}, [resolution_xy*resolution_xy,1]);
   
    %disp(size(BZ_slice{loop_num}));
    
    normB=sqrt(BX.^2+BY.^2+BZ.^2);
    norms{loop_num} = normB;
    
end  
%%
bz_min=-2e-5;
bz_max=2e-5;
figure(2);
view(2), axis tight
BZ_fig = zeros(resolution_xy,resolution_xy,num_loops);
BZ_total = zeros(resolution_xy,resolution_xy);
for loop_num = 1:num_loops
    subplot(2, num_loops/2,loop_num);
    BZ_fig(:, :, loop_num) = BZ_slice{loop_num};
    minBZ = min(BZ_slice{loop_num}, [], 'all');
    maxBZ = max(BZ_slice{loop_num}, [], 'all');
%     disp([minBZ, maxBZ]);
    imshow(BZ_slice{loop_num}, []);
    colormap('hot');
    colorbar
    BZ_total = BZ_total + BZ_slice{loop_num};
end

% imshow3(BZ_fig,[bz_min bz_max] , [2,num_loops/2]);%
% imshow(BZ_total,[bz_min bz_max]);
%plot_title=sprintf('loop num = %d',loop_num);
% title(plot_title,'FontSize',5); 
% hold on, grid on, box on, axis equal
% % xlabel('x [m]','FontSize',5), ylabel('y [m]','FontSize',5) % delete to make subplots look bigger
% view(2), axis tight
%% 
figure;
imshow(BZ_total,[]);
colormap('hot');
colorbar;
%% QUIVER
figure(1);
for loop_num = 1:num_loops
    subplot(2, num_loops/2,loop_num);
    scale=0.5;
    stepsize = 10;
    temp_norm = norms{loop_num};
    field = fields{loop_num};
    
    quiver(field.X(1:stepsize:end,1:stepsize:end,1),field.Y(1:stepsize:end,1:stepsize:end,1), ...
    field.BX(1:stepsize:end,1:stepsize:end,1)./temp_norm(1:stepsize:end,1:stepsize:end,1), ...
    field.BY(1:stepsize:end,1:stepsize:end,1)./temp_norm(1:stepsize:end,1:stepsize:end,1), ...
    scale);

    plot_title=sprintf('loop num = %d',loop_num);
    title(plot_title,'FontSize',5); 
    hold on, box on, axis equal, axis tight
end
%%
%least squares
phasemap = load("./data/fieldmap_9-23_1.mat");
hertz_to_tesla_conversion = 42570000;
transposed_fieldmap = transpose(phasemap.fieldmap);

fieldmap = angle(exp(1j*phasemap.fieldmap).*exp(-1j*pi/2))/0.0002/2/pi+1250;
fieldmap_mask = abs(phasemap.fieldmap) > mean(abs(phasemap.fieldmap), 'all')*.875;
% fieldmap = (unwrap(angle(phasemap.fieldmap), [], 2))/hertz_to_tesla_conversion;
field1_mask = fieldmap(fieldmap_mask);
field1_mask_mean = mean(abs(field1_mask), 'all');
temp = fieldmap.*fieldmap_mask;

% fieldmap(~fieldmap_mask) = 0;
figure;
subplot(2,2,1);
imshow(abs(phasemap.fieldmap), []);
subplot(2,2,2);
imshow(fieldmap, []);
subplot(2,2,3);
imshow(fieldmap_mask, [])
subplot(2,2,4);
imshow(temp, []);

least_sq_b = (field1_mask);
mask_size = length(least_sq_b);


loop_matrix_masked = zeros(mask_size, 6);
total = zeros(resolution_xy, resolution_xy);
for i = 1:6
    temp_loop = loop_matrix(:,i);
    total = total + reshape(temp_loop, resolution_xy, resolution_xy).*(fieldmap_mask);
    loop_matrix_masked(:,i) = temp_loop(fieldmap_mask);
end

figure;
imshow(total, []);


norm_loops = loop_matrix_masked;

%%
figure;
imshow(abs(phasemap.fieldmap));
colormap hot;
colorbar;
%%
cvx_begin
     cvx_solver sedumi
     variable x(6)
%      minimize(norm(norm_loops*x - least_sq_b, 4))
     minimize(sum_square(norm_loops*x - least_sq_b));
     x <= 1;
     x >= -1;
cvx_end
%% Shim before-after plotting
figure;
shimmed = reshape(loop_matrix*(x), [resolution_xy, resolution_xy]);
% [fin_y-start_y+1, fin_x-start_x+1]);
subplot(1, 3, 1);
imshow(shimmed, []);
colormap hot;
colorbar;
subplot(1, 3, 2);
field2 = reshape(fieldmap, [resolution_xy, resolution_xy]);
% [fin_y-start_y+1, fin_x-start_x+1]);
imshow(field2, []);
colormap hot;
colorbar;
subplot(1, 3, 3);
field2 = shimmed - fieldmap;
% [fin_y-start_y+1, fin_x-start_x+1]);
imshow(field2, []);
colormap hot;
colorbar;

%% Histogram plotting
figure;

% hist([norm_loops*x-least_sq_b, least_sq_b], 100)
hist([norm_loops*x-least_sq_b, least_sq_b], 100);
