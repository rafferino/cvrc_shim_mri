function [phantom_fieldmap, least_sq_b, loop_matrix, loop_matrix_masked, mask] = load_magnetic_loops(dirname, dims, slice_num, iterative, p)
    %LOAD_MAGNETIC_LOOPS Summary of this function goes here
    %   Detailed explanation goes here
    
    
    %load files from directory
    fieldmap_files = dir(char(dirname+'/loop*.mat'));
    num_loops = length(fieldmap_files);
    num_linear = 2;
    
    %load phantom fieldmap
    phantom_fieldmap_struct = load(dirname+'/nocurrent_Fieldmap.mat');
    centerFreq = phantom_fieldmap_struct.Parameters.PersistentParameters.Frequency_Hz.Value;
    hertz_to_tesla_conversion = 42570000;
    temp_phantom_fieldmap = (phantom_fieldmap_struct.PhaseMap(:,:,slice_num) + centerFreq);
    phantom_fieldmap = temp_phantom_fieldmap - mean(mean(temp_phantom_fieldmap));
%     boundary_value = mode(mode(phantom_fieldmap));
%     mask = phantom_fieldmap ~= boundary_value;
    magnitude = phantom_fieldmap_struct.Magnitude(:,:,slice_num);
    
    % compute convex hull of magnitude file to get proper threshold value
    hull = bwconvhull(imfill(edge(magnitude), 'holes'));
    threshold = max(magnitude(~hull)) - 2*std(magnitude(~hull));
    mask = magnitude > threshold;
    figure(7);
    imshow(mask, [])
    least_sq_b = phantom_fieldmap(mask);
%     least_sq_b = least_sq_b - mean(least_sq_b);
    
    %initialize dimensions and empty loop_matrix
    resolution_x = dims(1);
    resolution_y = dims(2);
    loop_matrix = zeros(resolution_x*resolution_y, num_loops+num_linear);
    loop_matrix_masked = zeros(length(least_sq_b), num_loops+num_linear);
    
    %vectorize all fieldmaps into loop_matrix
    if (p)
        figure(1);
        imshow(phantom_fieldmap, []);
        title("no current");
        colormap bone;
        colorbar;
    end
    
    low_phase = 1000000;
    high_phase = -100000;
    
    low_mag = 100000;
    high_mag = -100000;
    
    for loop_num = 1:length(fieldmap_files)
        fieldmap_files(loop_num).name
        loop_fieldmap_struct = load(dirname+"/"+fieldmap_files(loop_num).name);
        temp_fieldmap = loop_fieldmap_struct.PhaseMap(:,:,slice_num) + centerFreq;
        loop_fieldmap = (temp_fieldmap - temp_phantom_fieldmap);
%         loop_fieldmap_masked = (temp_fieldmap(mask) - least_sq_b);
        loop_fieldmap_masked = loop_fieldmap(mask);
        loop_matrix(:,loop_num) = reshape(loop_fieldmap, [resolution_x*resolution_y,1]);
        loop_matrix_masked(:, loop_num) = loop_fieldmap_masked;
        
        low_phase = min(low_phase, min(min(loop_fieldmap_masked)));
        high_phase = max(high_phase, max(max(loop_fieldmap_masked)));
        
        low_mag = min(low_mag, min(min(loop_fieldmap_struct.Magnitude(:,:,slice_num))));
        high_mag = max(high_mag, max(max(loop_fieldmap_struct.Magnitude(:,:,slice_num))));
        
    end
    
    y_gradient = transpose(linspace(0, 1, resolution_y))*ones(1,resolution_y) - mean(linspace(0, resolution_y));
    x_gradient = ones(resolution_x,1)*linspace(0, 1, resolution_x) - mean(linspace(0, resolution_x));
    
    loop_matrix(:, num_loops+1) = reshape(x_gradient, [resolution_x*resolution_y,1]);
    loop_matrix(:, num_loops+2) = reshape(y_gradient, [resolution_x*resolution_y,1]);
    
    loop_matrix_masked(:,num_loops+1) = x_gradient(mask);
    loop_matrix_masked(:,num_loops+2) = y_gradient(mask);
    
    
   if (p)
       for loop_num = 1:length(fieldmap_files)
            fieldmap_files(loop_num).name
            loop_fieldmap_struct = load(dirname+"/"+fieldmap_files(loop_num).name);
            temp_fieldmap = (loop_fieldmap_struct.PhaseMap(:,:,slice_num) + centerFreq);
            loop_fieldmap_disp = zeros(size(temp_fieldmap));
            loop_fieldmap = (temp_fieldmap - temp_phantom_fieldmap);
            loop_fieldmap_disp(mask) = (loop_fieldmap(mask));
            
            figure(2);
            subplot(2, floor(num_loops/2), loop_num); 
            imshow(loop_fieldmap_disp + (~mask .* -500), [low_phase high_phase]);
%             title(fieldmap_files(loop_num).name);
            colormap jet;
            
%             figure(4);
%             subplot(2, floor(num_loops/2), loop_num); 
%             imshow(loop_fieldmap_struct.Magnitude(:,:,slice_num), [low_mag, high_mag]);
%             title("Magnitude: Loop "+loop_num);
%             colormap bone;
%             colorbar;
        end 
            colorbar;
            set(gca, 'FontSize', 16);
   end
    
   if (iterative ~= "")
        iterative_fieldmap_struct = load(iterative);
        centerFreq = iterative_fieldmap_struct.Parameters.PersistentParameters.Frequency_Hz.Value;
        hertz_to_tesla_conversion = 42570000;
        temp_iterative_fieldmap = (iterative_fieldmap_struct.PhaseMap(:,:,slice_num) + centerFreq);
        phantom_fieldmap = temp_iterative_fieldmap - mean(mean(temp_iterative_fieldmap));
        least_sq_b = temp_iterative_fieldmap(mask);
        least_sq_b = least_sq_b - mean(least_sq_b);
   end
    
    
    
end

