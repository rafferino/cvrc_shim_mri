function [loop_matrix, loop_sims] = simulate_magnetic_loops(loops, dims, slice_num, p)
    %SIMULATE_LOOPS Simulate magnetic loops on the dimensions of the sleeve
    %   Uses BSmag library with a 1A current to simulate each individual
    %   loop, reshape loop simulation into a num_samples x num_samples
    %   length vector, and return it in the loop_matrix variable.
    
    I = 1; % filament current [A]
    dGamma = 1e9; % filament max discretization step [m]
    
    % Define parameters of physical sleeve
    x_dim = .1;
    y_dim = .1;
    z_dim = .05; % Coronal
    resolution_x = dims(1);
    resolution_y = dims(2);
    resolution_z = dims(3);
    [~,~,num_loops] = size(loops);
    loop_matrix = zeros(resolution_x*resolution_y, num_loops);
    
    % initialize matrices to return
    loop_sims = cell(1, num_loops);
    
    for loop_num = 1:num_loops
        BSmag.Nfilament = 0;
        
        % add simulated filaments from loop matrix
        [BSmag] = BSmag_add_filament(BSmag,loops(:,:,loop_num),I,dGamma);
        
        assignin('base', 'BSmag', BSmag);

        % Field points (where we want to calculate the field)
        x_M = linspace(-x_dim/2,x_dim/2,resolution_y); % x [m]
        y_M = linspace(-y_dim/2,y_dim/2,resolution_x); % y [m]
        z_M = linspace(-z_dim/2,z_dim/2,resolution_z); % z [m]
        [X_M,Y_M,Z_M]=meshgrid(x_M,y_M,z_M);
        %BSmag_plot_field_points(BSmag,X_M,Y_M,Z_M); % shows the field points volume

        % Get simulated magnetic field on x-y-z axes.
        [BSmag,X,Y,Z,BX,BY,BZ] = BSmag_get_B(BSmag,X_M,Y_M,Z_M);
        sim.BSmag = BSmag;
        sim.X = X;
        sim.Y = Y;
        sim.Z = Z;
        sim.BX = BX;
        sim.BY = BY; 
        sim.BZ = BZ;
        size(BZ)
        loop_sims{loop_num} = sim;
        
        %reshaping square matrix to vector for optimization
        loop_matrix(:,loop_num) = reshape(BZ(:,:,slice_num), [resolution_x*resolution_y,1]);

    end  
    
    %plot magnetic field if p
    if (p)
        figure;
        BZ_total = zeros(resolution_x,resolution_y);
        size(BZ_total)
        for loop_num = 1:num_loops
            subplot(2, num_loops/2,loop_num);
            imshow(loop_sims{loop_num}.BZ(:,:,slice_num), []);
            colormap('hot');
            colorbar;
            size(loop_sims{loop_num}.BZ(:,:,slice_num))
            BZ_total = BZ_total + loop_sims{loop_num}.BZ(:,:,slice_num);
        end
        figure;
        imshow(BZ_total, []);
    end
end

