function [fieldmap, loop_matrix_masked, least_sq_b, fieldmap_mask] = loop_fieldmap_masking(loop_matrix, data, slice_num)

    %LOOP_FIELDMAP_MASKING Masks fieldmap using convex hull and
    %thresholding
    %   Takes as input the fieldmap data (with structs "PhaseMap" and
    %   "Magnitude") and computes a convex hull around the edges of the
    %   phantom and masks the resulting bitmap using the 
    %%
    
    % get dimensions of PhaseMap
    [resolution_x, resolution_y, num_slices] = size(data.PhaseMap);
    [~, num_loops] = size(loop_matrix);
    %uncomment when field map is originally in radians
    hertz_to_tesla_conversion = 42570000;
    %radians_to_hertz_conversion = 1000/(2*pi); %this is only if the TE difference is 1 ms
    
    % get phasemap and magnitude slices
    % convert from radians to hertz to tesla
    centerFreq = data.Parameters.PersistentParameters.Frequency_Hz.Value;
    fieldmap = (data.PhaseMap(:,:,slice_num) + centerFreq)/hertz_to_tesla_conversion;
    fieldmap = fieldmap - mean(fieldmap, 'all');
    % fieldmap = (data.PhaseMap(:,:,8))/1e4;
    magnitude = data.Magnitude(:,:,slice_num);
    
    % compute convex hull of magnitude file to get proper threshold value
    hull = bwconvhull(imfill(edge(magnitude), 'holes'));
    figure;
    imshow(edge(magnitude), []);
%     threshold = max(magnitude.*(~hull), [], 'all')
    threshold = mode(fieldmap, 'all')
%     threshold = 0.0165;
%     threshold = mean(magnitude.*(~hull), 'all');
    %figure;
    %imshow(magnitude.*(~hull),[]);
%     fieldmap_mask = magnitude > threshold;
    fieldmap_mask = fieldmap ~= threshold;
    field1_masked = fieldmap(fieldmap_mask);
    temp = fieldmap.*fieldmap_mask;

    % apply fieldmap and plot for sanity check
    figure('Name', 'Fieldmap magnitude');
    subplot(2,2,1);
    imshow(magnitude, []);
    title("magnitude");
    colorbar;
    subplot(2,2,2);
    imshow(fieldmap, []);
    title("fieldmap");
    colorbar;
    subplot(2,2,3);
    imshow(fieldmap_mask, [])
    title("masked fieldmap");
    colorbar;
    subplot(2,2,4);
    imshow(hull, []);
    title("hull");
    colorbar;
    colormap bone;

    least_sq_b = field1_masked;
    mask_size = length(least_sq_b);

    % mask simulated loop matrices and plot for sanity check
    loop_matrix_masked = zeros(mask_size, num_loops);
    total = zeros(resolution_x, resolution_y);
    
    % boundaries for plotting
    low = 100000;
    high = -100000;
    
    for i = 1:num_loops
        temp_loop = reshape(loop_matrix(:,i), resolution_x, resolution_y);
        total = total + reshape(temp_loop, resolution_x, resolution_y).*(fieldmap_mask);
        loop_matrix_masked(:,i) = temp_loop(fieldmap_mask);
        
        low = min(low, min(temp_loop, [], 'all'))/2;
        high = max(high, max(temp_loop, [], 'all'))/2;
    end
    
    figure('Name', 'Loops');
    for i = 1:num_loops
        temp_loop = reshape(loop_matrix(:,i), resolution_x, resolution_y);
        subplot(2, num_loops/2, i);
        imshow(temp_loop.*fieldmap_mask, [low high]);
        colormap bone;
        colorbar;
    end

    figure('Name', 'All Loops');
    imshow(total, []);
    
end

