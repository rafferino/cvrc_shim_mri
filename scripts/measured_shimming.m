
slice_num = 32;

struct_1A = load("./data/fieldmaps/fieldmap_1A.mat");
struct_025A = load("./data/fieldmaps/fieldmap_025A.mat");
struct_0A = load("./data/fieldmaps/fieldmap_0A.mat");

% fieldmap_1A = struct_1A.PhaseMap(:,:,slice_num);
% fieldmap_025A = struct_025A.PhaseMap(:,:,slice_num);
% fieldmap_0A = struct_0A.PhaseMap(:,:,slice_num);

fieldmap_1A = reshape(struct_1A.PhaseMap(:,slice_num,:), 128, 80);
fieldmap_025A = reshape(struct_025A.PhaseMap(:,slice_num,:), 128, 80);
fieldmap_0A = reshape(struct_0A.PhaseMap(:,slice_num,:), 128, 80);

figure;
subplot(1,3,1);
imshow(fieldmap_1A - fieldmap_0A, []);
title("1A shimming");
colorbar;

subplot(1,3,2);
imshow(fieldmap_025A - fieldmap_0A, []);
title("0.25A shimming");
colorbar;

subplot(1,3,3);
imshow(fieldmap_0A, []);
title("no shimming");
colorbar;