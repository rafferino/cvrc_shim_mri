data = load("./data/DataStruct.mat");
p = data.PhaseMap;
m = data.Magnitude;

se = strel('disk',10);
hull = bwconvhull(imfill(edge(m(:,:,8)), 'holes'));

figure;
subplot(1,3,1);
imshow(hull, []);
subplot(1,3,2);
imshow(m(:,:,8).*hull,[]);

threshold = mean(m(:,:,8).*hull, 'all');
mask = m(:,:,8) > threshold;
subplot(1,3,3);
imshow(imfill(p(:,:,8).*mask), []);



