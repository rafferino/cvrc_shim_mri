close all;
clear all;

%%
data = load("./data/10-29_2 scans/nocurrent_Fieldmap.mat");
% data = load("./data/DataStruct.mat");
[resolution_x, resolution_y, num_slices] = size(data.PhaseMap);
slice_num = 40;%floor(num_slices/2)
%% Create loops
Gamma = generate_even_loops(.025, [3, 2]);
% Gamma = generate_loops([-0.025, -0.025, .025; .03, .01, .03]);

%% Simulate magnetic field in loops
[loop_matrix, loop_sims] = simulate_magnetic_loops(Gamma, size(data.PhaseMap), 4, true);

%% Mask and fit the field map to the loops
[fieldmap, loop_matrix_masked, least_sq_b, mask] = loop_fieldmap_masking(loop_matrix, data, slice_num);
x = loop_fieldmap_fitting(loop_matrix_masked, least_sq_b);

%% Shim before-after plotting
% calculate loaded loop induced magnetic fields
loop_fieldmap = reshape(loop_matrix*(x), [resolution_x, resolution_y]).*mask;
original_fieldmap = reshape(fieldmap, [resolution_x, resolution_y]).*mask;
shimmed = -loop_fieldmap + fieldmap; %the full difference

% calculate min and max for plotting bounds
low = min(min(fieldmap,[],'all'), min(loop_fieldmap+fieldmap,[],'all'))/4;
high = max(max(fieldmap,[],'all'), max(loop_fieldmap+fieldmap,[],'all'))/4;

% plotting
figure('Name', 'Shimming');
subplot(1, 3, 1);
imshow(loop_fieldmap, [low high]);
title("Loop Generated Fieldmap");
colormap bone;
colorbar;
subplot(1, 3, 2);
imshow(original_fieldmap, [low high]);
title("Original Fieldmap");
colormap bone;
colorbar;
subplot(1, 3, 3);
imshow(shimmed, [low high]);
title("Sum");
colormap bone;
colorbar;

%% Histogram plotting

% hist([norm_loops*x-least_sq_b, least_sq_b], 100)
%the masked difference between the fit values and the scan
figure;
subplot(1,3,1);
h1 = histogram(loop_matrix_masked*x-least_sq_b); %100 bins
h1.BinWidth
h1.BinLimits = [-10e-6, 10e-6];
h1.NumBins = 100;
h1.FaceColor = 'r';
h1.EdgeColor = 'none';
title("Fitted");

subplot(1,3,2);
h2 = histogram(least_sq_b);
h2.BinLimits = [-10e-6, 10e-6];
h2.NumBins = 100;
h2.FaceColor = 'b';
h2.EdgeColor = 'none';
title("Unfitted");

subplot(1,3,3);
h3 = histogram(loop_matrix_masked*x-least_sq_b);
h3.BinLimits = [-10e-6, 10e-6];
h3.NumBins = 100;
h3.FaceColor = 'r';
h3.EdgeColor = 'none';
hold on;
h4 = histogram(least_sq_b);
h4.BinLimits = [-10e-6, 10e-6];
h4.NumBins = 100;
h4.FaceColor = 'b';
h4.EdgeColor = 'none';
title("Both");

%% Figure making


