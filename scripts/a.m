%%
%least squares
run("shim_correction_final.m");
phasemap = load("data/bottle_fieldmap.mat");
hertz_to_tesla_conversion = 42570000;

fieldmap = angle(exp(1j*phasemap.small_img).*exp(-1j*pi/2))/0.0002/2/pi+1250;

field1 = transpose(squeeze(fieldmap)/hertz_to_tesla_conversion);
field1_mask_mean = mean(mean(field1_mask));
least_sq_b = reshape(field1_mask - field1_mask_mean,[mask_size,1]);

loop_matrix_masked = zeros(mask_size, 6);
total = zeros(length(mask_y), length(mask_x));
for i = 1:6
    temp = reshape(loop_matrix(:,i), 256, 256);
    temp_masked = temp(mask_y, mask_x);
    total = total+temp_masked;
    loop_matrix_masked(:,i) = reshape(temp_masked, mask_size, 1);
end

figure;
subplot(1,2,1);
imshow(field1(mask_y, mask_x), []);
colormap hot;
colorbar;
subplot(1,2,2);
imshow(total, []);
colormap hot;
colorbar;

figure;
imshow(fieldmap, []);

norm_loops = loop_matrix_masked;

%%
figure;
imshow(abs(phasemap.fieldmap));
colormap hot;
colorbar;
%%
cvx_begin
     cvx_solver sedumi
     variable x(6)
%      minimize(norm(norm_loops*x - least_sq_b, 4))
     minimize(sum_square(norm_loops*x - least_sq_b));
     x <= 1;
     x >= -1;
cvx_end
%% Shim before-after plotting
figure;
shimmed = reshape(loop_matrix*(x), [256, 256]);
% [fin_y-start_y+1, fin_x-start_x+1]);
subplot(1, 3, 1);
imshow(shimmed, []);
colormap hot;
subplot(1, 3, 2);
field2 = reshape(field1, [256, 256]);
% [fin_y-start_y+1, fin_x-start_x+1]);
imshow(field2, []);
colormap hot;
subplot(1, 3, 3);
field2 = shimmed - reshape(field1, [256, 256]);
% [fin_y-start_y+1, fin_x-start_x+1]);
imshow(field2, []);
colormap hot;

%% Histogram plotting
figure;
hist([norm_loops*x-least_sq_b, least_sq_b], 100)
