function [x] = loop_fieldmap_fitting(norm_loops,least_sq_b, fit)
    %LOOP_FIELDMAP_FITTING Run fitting on masked loops and slice data
    %   Using CVX optimization solver on masked simulated loops and masked
    %   slice data with L2 loss and a constraint between -1 and 1.
    
    [~, num_loops] = size(norm_loops);
    cvx_begin
        cvx_solver sedumi
        variable x(num_loops)
        % minimization over l2 norm
        if (fit == 1)
            minimize(sum(abs((norm_loops*x - least_sq_b))));
        end
        if (fit == 2)
            minimize(sum_square(norm_loops*x - least_sq_b));
        end
        if (fit == inf)
            minimize(norm(norm_loops*x - least_sq_b, inf));
        end
        
        %uncomment to constrain values
%         x <= 2;
%         x >= -2;
    cvx_end
end

