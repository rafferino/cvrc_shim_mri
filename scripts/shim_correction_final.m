%---------------------------------------------------
%  NAME:      example_3D_solenoid_filament.m
%  WHAT:      Calculation of the magnetic field of a solenoid
%             on a volume (+ 3D plot).
%  REQUIRED:  BSmag Toolbox 20150407
%  AUTHOR:    20150407, L. Queval (loic.queval@gmail.com)
%  COPYRIGHT: 2015, Loic Qu�val, BSD License (http://opensource.org/licenses/BSD-3-Clause).
%----------------------------------------------------
%load('~/Desktop/feb19_scan/DataStruct.mat');
% Initialize
clear all; 
close all;
clc;
% BSmag = BSmag_init(); % Initialize BSmag analysis

% Source points (where there is a current source)
%r = 1/5 + 0.03 * sin(4 * theta') + 0.05 * sin(2 * theta' - 0.5);
%Gamma = [r .* cos(theta'), r .* sin(theta'), theta'/10000]; % x,y,z [m,m,m]
num_loops = 6;
theta_resolution = 200;
theta = linspace(-pi,pi,theta_resolution);
Gamma = zeros(theta_resolution,3,num_loops);
size_divider = 40;
y_sep = 55;
near_offset = 30;
far_offset = 15;
Gamma(:,:,1) = [sin(theta')/size_divider + 1/(-near_offset), cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
Gamma(:,:,2) = [sin(theta')/size_divider, cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
Gamma(:,:,3) = [sin(theta')/size_divider + 1/(near_offset), cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
Gamma(:,:,4) = [sin(theta')/size_divider + 1/(-near_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
Gamma(:,:,5) = [sin(theta')/size_divider, cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
Gamma(:,:,6) = [sin(theta')/size_divider + 1/(near_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
% Gamma(:,:,7) = [sin(theta')/size_divider + 1/(far_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
% Gamma(:,:,8) = [sin(theta')/size_divider + 1/(near_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];

% size_divider = 45;
% y_sep = 55;
% near_offset = 30;
% far_offset = 15;
% Gamma(:,:,1) = [zeros(size(theta')), zeros(size(theta')), zeros(size(theta'))];
% Gamma(:,:,2) = [zeros(size(theta')), zeros(size(theta')), zeros(size(theta'))];
% Gamma(:,:,3) = [sin(theta')/size_divider + 1/(near_offset), cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
% Gamma(:,:,4) = [zeros(size(theta')), zeros(size(theta')), zeros(size(theta'))];
% Gamma(:,:,5) = [sin(theta')/size_divider, cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
% Gamma(:,:,6) = [zeros(size(theta')), zeros(size(theta')), zeros(size(theta'))];


I = 1; % filament current [A]
dGamma = 1e9; % filament max discretization step [m]
x_dim = .1;
y_dim = .1;
z_dim = .05;
resolution_z = 5;
resolution_y = 256;%2*resolution_z +1;
resolution_x = resolution_y;%used to be 4*resolution_z + 1;

loop_matrix = zeros(resolution_y*resolution_y, num_loops);
BZ_slice=cell(1,num_loops);
norms = cell(1, num_loops);
fields = cell(1, num_loops);
for loop_num = 1:num_loops
    BSmag.Nfilament = 0;
    [BSmag] = BSmag_add_filament(BSmag,Gamma(:,:,loop_num),I,dGamma);


    % Field points (where we want to calculate the field)
    x_M = linspace(-x_dim/2,x_dim/2,resolution_x); % x [m]
    y_M = linspace(-y_dim/2,y_dim/2,resolution_y); % y [m]
    z_M = linspace(-z_dim/2,z_dim/2,resolution_z); % z [m]
    [X_M,Y_M,Z_M]=meshgrid(x_M,y_M,z_M);
%     BSmag_plot_field_points(BSmag,X_M,Y_M,Z_M); % shows the field points volume

    % Biot-Savart Integration
    [BSmag,X,Y,Z,BX,BY,BZ] = BSmag_get_B(BSmag,X_M,Y_M,Z_M);
    field.BSmag = BSmag;
    field.X = X;
    field.Y = Y;
    field.Z = Z;
    field.BX = BX;
    field.BY = BY; 
    field.BZ = BZ;
    fields{loop_num} = field;
    size(BZ(:,:,4))
    BZ_slice{loop_num} = BZ(:,1:256,4);
    figure;
    imshow(BZ_slice{loop_num}, []);
    
    
    
    loop_matrix(:,loop_num) = reshape(BZ_slice{loop_num}, [resolution_y*resolution_y,1]);
   
    %disp(size(BZ_slice{loop_num}));
    
    normB=sqrt(BX.^2+BY.^2+BZ.^2);
    norms{loop_num} = normB;
    
end  
%%
bz_min=-2e-5;
bz_max=2e-5;
figure(2);
view(2), axis tight
BZ_fig = zeros(resolution_y,resolution_y,num_loops);
BZ_total = zeros(resolution_y,resolution_y);
for loop_num = 1:num_loops
    subplot(2, num_loops/2,loop_num);
    BZ_fig(:, :, loop_num) = BZ_slice{loop_num};
    minBZ = min(BZ_slice{loop_num}, [], 'all');
    maxBZ = max(BZ_slice{loop_num}, [], 'all');
%     disp([minBZ, maxBZ]);
    imshow(BZ_slice{loop_num}, []);
    colormap('hot');
    colorbar
    BZ_total = BZ_total + BZ_slice{loop_num};
end

% imshow3(BZ_fig,[bz_min bz_max] , [2,num_loops/2]);%
% imshow(BZ_total,[bz_min bz_max]);
%plot_title=sprintf('loop num = %d',loop_num);
% title(plot_title,'FontSize',5); 
% hold on, grid on, box on, axis equal
% % xlabel('x [m]','FontSize',5), ylabel('y [m]','FontSize',5) % delete to make subplots look bigger
% view(2), axis tight
%% 
figure(3)
imshow(BZ_total,[]);
colormap('hot');
colorbar;
%% QUIVER
figure(1);
for loop_num = 1:num_loops
    subplot(2, num_loops/2,loop_num);
    scale=0.5;
    stepsize = 10;
    temp_norm = norms{loop_num};
    field = fields{loop_num};
    
    quiver(field.X(1:stepsize:end,1:stepsize:end,1),field.Y(1:stepsize:end,1:stepsize:end,1), ...
    field.BX(1:stepsize:end,1:stepsize:end,1)./temp_norm(1:stepsize:end,1:stepsize:end,1), ...
    field.BY(1:stepsize:end,1:stepsize:end,1)./temp_norm(1:stepsize:end,1:stepsize:end,1), ...
    scale);

    plot_title=sprintf('loop num = %d',loop_num);
    title(plot_title,'FontSize',5); 
    hold on, box on, axis equal, axis tight
end
%%
%least squares
phasemap = load("./data/fieldmap.mat");
hertz_to_tesla_conversion = 42570000;

fieldmap = angle(exp(1j*phasemap.fieldmap).* exp(-1j*pi/2))/0.0002/2/pi+1250;
% fieldmap = unwrap(angle(exp(1j*phasemap.fieldmap).*exp(-1j*pi/2)));
% figure(2);
% imshow(unwrap(angle(transpose(phasemap.fieldmap)), [], 2), []);

start_x = 25;
fin_x = 231;

start_y = 30;
fin_y = 210;

mask_x = start_x:fin_x;
mask_y = start_y:fin_y;
mask_size = length(mask_x)*length(mask_y);
field1 = transpose(squeeze(fieldmap)/hertz_to_tesla_conversion);
field1_mean = mean(mean(field1));
norm_field1 = field1/field1_mean;
field1_mask = field1(mask_y, mask_x);
field1_mask_mean = mean(mean(field1_mask));
least_sq_b = reshape(field1_mask - field1_mask_mean,[mask_size,1]);

loop_matrix_masked = zeros(mask_size, 6);
total = zeros(length(mask_y), length(mask_x));
for i = 1:6
    temp = reshape(loop_matrix(:,i), 256, 256);
    temp_masked = temp(mask_y, mask_x);
    total = total+temp_masked;
    loop_matrix_masked(:,i) = reshape(temp_masked, mask_size, 1);
end

figure;
subplot(1,2,1);
imshow(field1(mask_y, mask_x), []);
colormap hot;
colorbar;
subplot(1,2,2);
imshow(total, []);
colormap hot;
colorbar;

figure;
imshow(fieldmap, []);

norm_loops = loop_matrix_masked;

%%
figure;
imshow(abs(phasemap.fieldmap));
colormap hot;
colorbar;
%%
cvx_begin
     cvx_solver sedumi
     variable x(6)
%      minimize(norm(norm_loops*x - least_sq_b, 4))
     minimize(sum_square(norm_loops*x - least_sq_b));
     x <= 1;
     x >= -1;
cvx_end
%% Shim before-after plotting
figure;
shimmed = reshape(loop_matrix*(x), [256, 256]);
% [fin_y-start_y+1, fin_x-start_x+1]);
subplot(1, 3, 1);
imshow(shimmed, []);
colormap hot;
subplot(1, 3, 2);
field2 = reshape(field1, [256, 256]);
% [fin_y-start_y+1, fin_x-start_x+1]);
imshow(field2, []);
colormap hot;
subplot(1, 3, 3);
field2 = shimmed - reshape(field1, [256, 256]);
% [fin_y-start_y+1, fin_x-start_x+1]);
imshow(field2, []);
colormap hot;

%% Histogram plotting
figure;
hist([norm_loops*x-least_sq_b, least_sq_b], 100)
