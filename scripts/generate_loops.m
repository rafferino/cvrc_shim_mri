function [Gamma] = generate_loops(loop_info)
    %GENERATE_LOOPS generates loops at specific points [-0.1, 0.1] and
    %radii on the sleeve
    
    % loop info elements should be formatted as such: [x, y, radius]
    % number of loops
    [num_loops, ~] = size(loop_info);
    
    % define points on the loop
    theta_resolution = 200;
    theta = linspace(-pi,pi,theta_resolution);
    Gamma = zeros(theta_resolution,3,num_loops);
    for i = 1:num_loops
%         [x, y, radius] = loop_info(:, i);
        Gamma(:,:,i) = [sin(theta')*loop_info(i, 3) + loop_info(i,1), cos(theta')*loop_info(i,3) + loop_info(i,2), zeros(size(theta'))];
    end
end

