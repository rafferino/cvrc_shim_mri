function [Gamma] = generate_even_loops(radius, dims)
    %GENERATE_LOOPS Generate loops of specified radius and spread
    %   Places loops on a linspace from the edge of the sleeve to the other
    %   edge
    
    % dimensions of sleeve
    x_dim = .1;
    y_dim = .1;
    
    
    % define points on the loop
    theta_resolution = 200;
    theta = linspace(-pi,pi,theta_resolution);
    Gamma = zeros(theta_resolution,3,prod(dims));
    
    % create evenly spaced points for center of loops
    m = linspace((-x_dim/2) + radius, (x_dim/2) - radius, dims(1));
    n = linspace((-y_dim/2) + radius, (y_dim/2) - radius, dims(2));
    
    % create 200 point loops
    count = 1;
    for i = 1:length(m)
        for j = 1:length(n)
            Gamma(:,:,count) = [sin(theta')*radius + m(i), cos(theta')*radius + n(j), zeros(size(theta'))];
            count = count + 1;
        end
    end
end

