%%

Nx = 256;
Ny = 256;

F_small=fopen('data/4-19-slice8_te2-0.img');
F_large=fopen('data/4-19-slice8_te2-2.img');

small_te=fread(F_small,[1,inf],'double');
large_te=fread(F_large,[1,inf],'double');

fclose(F_small);
fclose(F_large);

small_te=small_te(1:2:end)+1i*small_te(2:2:end);
large_te=large_te(1:2:end)+1i*large_te(2:2:end);

small_te=reshape(small_te,Nx,Ny);
large_te=reshape(large_te,Nx,Ny);
%%
figure();
imshow(abs(small_te));
%%

small_img=(small_te);
large_img=(large_te);

figure();
subplot(1,2,1);
imshow(abs(small_img), []);
subplot(1,2,2);
imshow(abs(fft2c(small_img)), []);

figure();
subplot(1,2,1);
imshow(abs(large_img), []);
subplot(1,2,2);
imshow(abs(fft2c(large_img)), []);

%%
figure();
fieldmap = angle(large_te.*conj(small_te));
imshow(fieldmap, []);
save('fieldmap.mat', 'fieldmap');

%%
% F_arda=fopen('arda_field_map.mat');
arda_mat=load('arda_field_map.mat');
arda_te=arda_mat.unwrapped_output_map(:,:,3);
figure();
imshow(arda_te, []);


