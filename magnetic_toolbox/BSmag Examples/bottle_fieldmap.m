%%

N = 128;

F_small=fopen('data/bottle_8-17-19.img');

small_te=fread(F_small,[1,inf],'double');

fclose(F_small);

small_te=small_te(1:2:end)+1i*small_te(2:2:end);

small_te=reshape(small_te,N,N);
%%
figure();
imshow(abs(small_te), []);
%%

PhaseMap=angle(small_te);
Magnitude=abs(small_te);

figure();
subplot(1,2,1);
imshow(abs(PhaseMap), []);
subplot(1,2,2);
imshow(abs(fft2c(PhaseMap)), []);
save("bottle_fieldmap_128", "PhaseMap", "Magnitude");

%%
% F_arda=fopen('arda_field_map.mat');
% arda_mat=load('arda_field_map.mat');
% arda_te=arda_mat.unwrapped_output_map(:,:,3);
% figure();
% imshow(arda_te, []);


