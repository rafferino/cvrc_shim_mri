%---------------------------------------------------
%  NAME:      example_3D_solenoid_filament.m
%  WHAT:      Calculation of the magnetic field of a solenoid
%             on a volume (+ 3D plot).
%  REQUIRED:  BSmag Toolbox 20150407
%  AUTHOR:    20150407, L. Queval (loic.queval@gmail.com)
%  COPYRIGHT: 2015, Loic Qu�val, BSD License (http://opensource.org/licenses/BSD-3-Clause).
%----------------------------------------------------

% Initialize
clear all, close all, clc
BSmag = BSmag_init(); % Initialize BSmag analysis

% Source points (where there is a current source)
theta = linspace(-pi,pi,2*100);
%r = 1/5 + 0.03 * sin(4 * theta') + 0.05 * sin(2 * theta' - 0.5);
%Gamma = [r .* cos(theta'), r .* sin(theta'), theta'/10000]; % x,y,z [m,m,m]
Gamma = zeros(200,3,8);
Gamma(:,:,1) = [sin(theta')/70 + 1/(-15), cos(theta')/70 + 1/(45), zeros(size(theta'))];
Gamma(:,:,2) = [sin(theta')/70 + 1/(-45), cos(theta')/70 + 1/(45), zeros(size(theta'))];
Gamma(:,:,3) = [sin(theta')/70 + 1/(45), cos(theta')/70 + 1/(45), zeros(size(theta'))];
Gamma(:,:,4) = [sin(theta')/70 + 1/(15), cos(theta')/70 + 1/(45), zeros(size(theta'))];
Gamma(:,:,5) = [sin(theta')/70 + 1/(-15), cos(theta')/70 + 1/(-45), zeros(size(theta'))];
Gamma(:,:,6) = [sin(theta')/70 + 1/(-45), cos(theta')/70 + 1/(-45), zeros(size(theta'))];
Gamma(:,:,7) = [sin(theta')/70 + 1/(45), cos(theta')/70 + 1/(-45), zeros(size(theta'))];
Gamma(:,:,8) = [sin(theta')/70 + 1/(15), cos(theta')/70 + 1/(-45), zeros(size(theta'))];




I = 1; % filament current [A]
dGamma = 1e9; % filament max discretization step [m]
loop_matrix = zeros(31, 8);
for loop_num = 1:8
    BSmag = BSmag_init();
    [BSmag] = BSmag_add_filament(BSmag,Gamma(:,:,loop_num),I,dGamma);


    % Field points (where we want to calculate the field)
    x_M = linspace(-.15,.15,31); % x [m]
    y_M = linspace(-.15,.15,31); % y [m]
    z_M = linspace(-.15,.15,31); % z [m]
    [X_M,Y_M,Z_M]=meshgrid(x_M,y_M,z_M);
    BSmag_plot_field_points(BSmag,X_M,Y_M,Z_M); % shows the field points volume

    % Biot-Savart Integration
    [BSmag,X,Y,Z,BX,BY,BZ] = BSmag_get_B(BSmag,X_M,Y_M,Z_M);
    loop_matrix(:,loop_num) = BZ(16,16,:);
end  
%least squares
b = linspace(1e-6, 1e-7,31).';
%b = linear_func' .* linear_func';
current = loop_matrix\b;


%plotting not accurate

% Plot B/|B|
figure(1)
    normB=sqrt(BX.^2+BY.^2+BZ.^2);
    quiver3(X,Y,Z,BX./normB,BY./normB,BZ./normB,'b')
%axis tight

% Plot Bz on the volume
figure(2), hold on, box on, grid on
	%plot3(Gamma_1(:,1),Gamma_1(:,2),Gamma_1(:,3),'.-r') % plot filament
	slice(X,Y,Z,BZ,[0],[],[-1,0,1]), colorbar % plot Bz
view(3), axis equal, axis tight
caxis([-0.5,0.5]*1e-5)

% Plot some flux tubes
figure(3), hold on, box on, grid on
	%plot3(Gamma_1(:,1),Gamma_1(:,2),Gamma_1(:,3),'.-r') % plot filament
	[X0,Y0,Z0] = ndgrid(-1.5:0.5:1.5,-1.5:0.5:1.5,-2); % define tubes starting point        
	htubes = streamtube(stream3(X,Y,Z,BX,BY,BZ,X0,Y0,Z0), [0.2 10]);
xlabel ('x [m]'), ylabel ('y [m]'), zlabel ('z [m]'), title ('Some flux tubes')
view(3), axis equal, axis tight
set(htubes,'EdgeColor','none','FaceColor','c') % change tube color
camlight left % change tube light