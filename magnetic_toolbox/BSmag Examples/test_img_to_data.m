%% Reconstruct the results:
F=fopen('4-19-raw-slice8_te2-0.fid');
A3=fread(F,[1,inf],'double');
fclose(F);
A3=A3(1:2:end)+1i*A3(2:2:end);
Nx = 220;
Ny = 220;
A3=reshape(A3,Nx,Ny);
[value, index] = max(A3(:,end/2+1))
padded = zeros(318,220);
padded(99:318,:) = A3;
A3 = padded;
[value, index] = max(A3(:,end/2+1))
figure(4)
te_3 = (ifft2c(A3));
subplot(1, 2, 1);
imshow(log(abs(A3)), []);
subplot(1,2,2);
imshow(angle(te_3), []);


%%
F=fopen('4-19-raw-slice8_te2-2.fid');
A4=fread(F,[1,inf],'double');
fclose(F);
A4=A4(1:2:end)+1i*A4(2:2:end);
A4=reshape(A4,Nx,Ny);
padded = zeros(318,220);
padded(99:318,:) = A4;
A4 = padded;
figure(5)
te_4 = (ifft2c(A4));
subplot(1,2,1);
imshow(abs(A4), []);
subplot(1,2,2);
imshow(angle(te_4), []);
% subplot(1,3,3);
% imshow(abs(fftshift(te_4)), []);

%%
figure(6);
% subplot(1,2,1);
phase = angle(A3*conj(A4));
imshow(phase, []);


%%
figure(7);
C = fft2(A3);
C = fftshift(C);
C = ifft2(C);
% C = fftshift(C);
imshow(angle(C), []);
figure(8);
imshow(abs(C), []);
% subplot(1,2,2);
% phase = (te_3*conj(te_4));
% imshow(phase);

%%
figure(9);
D = fft2(A4);
D = fftshift(D);
D = ifft2(D);
% C = fftshift(C);
imshow(angle(D), []);
figure(10);
imshow(abs(D), []);

%%
figure();
% subplot(1,2,1);
imshow(angle(ifft2c(A4).*conj(ifft2c(A3))), []);
% subplot(1,2,2);
% imshow(angle(A4*conj(A3)), [])

%%
figure();
imshow(abs(fftshift(fft2(fftshift(C)))), []);
figure();
imshow(abs(fftshift(fft2(fftshift(D)))), []);

%%
E = angle(D*conj(C));




