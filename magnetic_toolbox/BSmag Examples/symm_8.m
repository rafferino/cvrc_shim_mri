function [Gamma] = symm_8()
%8_SYMM Summary of this function goes here
%   Detailed explanation goes here
num_loops = 8;
theta_resolution = 200;
Gamma = zeros(theta_resolution,3,num_loops);
size_divider = 45;
y_sep = 55;
near_offset = 30;
far_offset = 15;
Gamma(:,:,1) = [sin(theta')/size_divider + 1/(-far_offset), cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
Gamma(:,:,2) = [sin(theta')/size_divider + 1/(-near_offset), cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
Gamma(:,:,3) = [sin(theta')/size_divider + 1/(near_offset), cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
Gamma(:,:,4) = [sin(theta')/size_divider + 1/(far_offset), cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
Gamma(:,:,5) = [sin(theta')/size_divider + 1/(-far_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
Gamma(:,:,6) = [sin(theta')/size_divider + 1/(-near_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
Gamma(:,:,7) = [sin(theta')/size_divider + 1/(near_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
Gamma(:,:,8) = [sin(theta')/size_divider + 1/(far_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];

end

