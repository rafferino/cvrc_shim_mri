function [Gamma] = symm_6()
%6_SYMM Summary of this function goes here
%   6-Loop Symmetric configuration for loops
num_loops = 6;
theta_resolution = 200;
Gamma = zeros(theta_resolution,3,num_loops);
size_divider = 45;
y_sep = 55;
near_offset = 30;
far_offset = 15;
Gamma(:,:,1) = [sin(theta')/size_divider + 1/(-near_offset), cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
Gamma(:,:,2) = [sin(theta')/size_divider, cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
Gamma(:,:,3) = [sin(theta')/size_divider + 1/(near_offset), cos(theta')/size_divider + 1/(y_sep), zeros(size(theta'))];
Gamma(:,:,4) = [sin(theta')/size_divider + 1/(-near_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
Gamma(:,:,5) = [sin(theta')/size_divider, cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
Gamma(:,:,6) = [sin(theta')/size_divider + 1/(near_offset), cos(theta')/size_divider + 1/(-y_sep), zeros(size(theta'))];
end

